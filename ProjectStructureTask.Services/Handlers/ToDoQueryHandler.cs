﻿using AutoMapper;
using ProjectStructureTask.Services.DTOs;
using Microsoft.EntityFrameworkCore;
using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using Threading = System.Threading.Tasks;
using ProjectStructureTask.Services.Repositories;

namespace ProjectStructureTask.Services.Handlers
{
    public class ToDoQueryHandler : IToDoQueryHandler
    {
        private readonly UnitOfWork dataContext;

        public ToDoQueryHandler(IUnitOfWork dataContext)
        {
            this.dataContext = dataContext as UnitOfWork;
        }


        public Dictionary<Type, Func<object, Threading.Task<object>>> GetHandlers()
        {
            return new Dictionary<Type, Func<object, Threading.Task<object>>>
            {
                { typeof(GetAllProjectsQuery), async t => await HandleAsync(t as GetAllProjectsQuery) },
                { typeof(GetAllTasksQuery), async t => await HandleAsync(t as GetAllTasksQuery) },
                { typeof(GetAllUsersQuery), async t => await HandleAsync(t as GetAllUsersQuery) },
                { typeof(GetAllTeamsQuery), async t => await HandleAsync(t as GetAllTeamsQuery) },
                { typeof(GetProjectQuery), async t => await HandleAsync(t as GetProjectQuery) },
                { typeof(GetTeamQuery), async t => await HandleAsync(t as GetTeamQuery) },
                { typeof(GetUserQuery), async t => await HandleAsync(t as GetUserQuery) },
                { typeof(GetTaskQuery), async t => await HandleAsync(t as GetTaskQuery) },
                { typeof(GetInfoAboutProjectQuery), async t => await HandleAsync(t as GetInfoAboutProjectQuery) },
                { typeof(GetInfoAboutLastProjectQuery), async t => await HandleAsync(t as GetInfoAboutLastProjectQuery) },
                { typeof(GetListTasksFinished2019Query), async t => await HandleAsync(t as GetListTasksFinished2019Query) },
                { typeof(GetListTasksQuery), async t => await HandleAsync(t as GetListTasksQuery) },
                { typeof(GetNumberTasksInProjectQuery), async t => await HandleAsync(t as GetNumberTasksInProjectQuery) },
                { typeof(GetOldUsersQuery), async t => await HandleAsync(t as GetOldUsersQuery) },
                { typeof(GetSortedUsersListQuery), async t => await HandleAsync(t as GetSortedUsersListQuery) },
                
            };
        }

        private async Threading.Task<List<Project>> HandleAsync(GetAllProjectsQuery query)
        {
            return await dataContext.Projects.ToListAsync();
        }

        private async Threading.Task<List<Task>> HandleAsync(GetAllTasksQuery query)
        {
            return await dataContext.Tasks.ToListAsync();
        }

        private async Threading.Task<List<User>> HandleAsync(GetAllUsersQuery query)
        {
            return await dataContext.Users.ToListAsync();
        }

        private async Threading.Task<List<Team>> HandleAsync(GetAllTeamsQuery query)
        {
            return await dataContext.Teams.ToListAsync();
        }

        private async Threading.Task<Project> HandleAsync(GetProjectQuery query)
        {
            return await Threading.Task.Run(() => 
                dataContext.Projects.Single(findProject => findProject.Id == query.Id));
        }

        private async Threading.Task<Team> HandleAsync(GetTeamQuery query)
        {
            return await Threading.Task.Run(() => 
                dataContext.Teams.Single(findTeam => findTeam.Id == query.Id));
        }

        private async Threading.Task<User> HandleAsync(GetUserQuery query)
        {
            return await Threading.Task.Run(() => 
                dataContext.Users.Single(findUser => findUser.Id == query.Id));
        }

        private async Threading.Task<Task> HandleAsync(GetTaskQuery query)
        {
            return await Threading.Task.Run(() => 
                dataContext.Tasks.Single(findTask => findTask.Id == query.Id));
        }

        private async Threading.Task<Dictionary<string, int>> HandleAsync(GetNumberTasksInProjectQuery query)
        {
            var resultQuery = await Threading.Task.Run(() => dataContext.Projects.GroupJoin(
                dataContext.Tasks.Where(t => t.Id == query.Id),
                p => p, t => t.Project,
                (project, task) => new
                {
                    project,
                    numberTasks = task.Count(t => t.Project.Id == project.Id)
                }));

            Dictionary<string, int> result = new Dictionary<string, int>();
            foreach (var item in resultQuery)
            {
                result.Add(item.project.Name, item.numberTasks);
            }

            return result;
        }

        private async Threading.Task<List<Task>> HandleAsync(GetListTasksQuery query)
        {
            return await Threading.Task.Run(() => dataContext.Tasks.Where(task => 
                task.Perfomer.Id == query.Id && task.Name.Length < 45).ToList());
        }

        private async Threading.Task<ICollection<Task>> HandleAsync(GetListTasksFinished2019Query query)
        {
            return await Threading.Task.Run(() => dataContext.Tasks.Where(task => task.Perfomer.Id == query.Id && 
                                                                                  task.State.Value == "Finished" &&
                                                                                  task.FinishedAt.Year == 2019)
                                                                   .ToList());
        }

        private async Threading.Task<IEnumerable<object>> HandleAsync(GetOldUsersQuery query)
        {
            var oldUsers = await Threading.Task.Run(() => dataContext.Teams.GroupJoin(
                dataContext.Users.Where(user => user.Birthday <= DateTime.Now.AddYears(-12) && user.Team != null)
                                 .OrderBy(user => user.RegisteredAt),
                t => t, u => u.Team,
                (team, user) => new
                {
                    team.Id,
                    team.Name,
                    Users = user.Select(u => u)
                }));

            return oldUsers;
        }

        private async Threading.Task<IEnumerable<Task>> HandleAsync(GetSortedUsersListQuery query)
        {
            return await Threading.Task.Run(() => 
                dataContext.Tasks.OrderBy(task => task.Name.Length)
                                 .ThenByDescending(task => task.Perfomer.FirstName));
        }

        private async Threading.Task<IEnumerable<object>> HandleAsync(GetInfoAboutLastProjectQuery query)
        {
            var result = await Threading.Task.Run(() => 
                         from user in dataContext.Users
                         where user.Id == query.Id
                         join project in dataContext.Projects on user.Id equals project.Author.Id
                         where project.CreatedAt == dataContext.Projects.Where(p => p.Author.Id == user.Id)
                                                            .Max(x => x.CreatedAt)
                         let lastProject = project
                         join task in dataContext.Tasks on project.Id equals task.Project.Id
                         let numberTasksInLastProject = dataContext.Tasks.Count(t => t.Project.Id == lastProject.Id)
                         let numberUnfinishedOrCanceledTasks = dataContext.Tasks.Count(t => 
                            t.Project.Id == lastProject.Id && task.State.Value != "Finished")
                         let longTask = dataContext.Tasks.Where(t => t.Project.Id == lastProject.Id)
                                             .Aggregate((x, y) => (x.FinishedAt - x.CreateAt) >
                                                                  (y.FinishedAt - y.CreateAt) ? x : y)
                         select new
                         {
                             user,
                             lastProject,
                             numberTasksInLastProject,
                             numberUnfinishedOrCanceledTasks,
                             longTask
                         });

            return result;
        }

        private async Threading.Task<IEnumerable<object>> HandleAsync(GetInfoAboutProjectQuery query)
        {
            var result = await Threading.Task.Run(() => 
                         from project in dataContext.Projects
                         where project.Id == query.Id
                         join task in dataContext.Tasks on project.Id equals task.Project.Id
                         let longDescriptionProject = dataContext.Tasks.Where(t => t.Project.Id == project.Id)
                                                           .Aggregate((x, y) => x.Description.Length >
                                                                                y.Description.Length ? x : y)
                         let shortNameTask = dataContext.Tasks.Where(t => t.Project.Id == project.Id)
                                                           .Aggregate((x, y) => x.Name.Length <
                                                                                y.Name.Length ? x : y)
                         let tasksInProject = dataContext.Tasks.Where(t => t.Project.Id == project.Id)
                         let numberUsersInProject = tasksInProject.Where(t => t.Project.Description.Length > 25 ||
                                                                              tasksInProject.Count() < 3)
                                                                  .Select(t => t.Perfomer).GroupBy(t => t.Id).Count()
                         select new
                         {
                             project,
                             longDescriptionProject,
                             shortNameTask,
                             numberUsersInProject
                         });

            return result;
        }
    }
}
