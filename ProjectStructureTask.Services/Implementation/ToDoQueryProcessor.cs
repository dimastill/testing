﻿using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.Handlers;
using ProjectStructureTask.Services.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructureTask.Services.Implementation
{
    public class ToDoQueryProcessor : Processor, IQueryProcessor
    {
        public ToDoQueryProcessor(IToDoQueryHandler handlerFactory)
        {
            RegisterHandlersAsync(handlerFactory);
        }
    }
}
