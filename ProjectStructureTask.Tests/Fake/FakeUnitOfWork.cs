﻿using Microsoft.EntityFrameworkCore;
using ProjectStructureTask.Services.DTOs;
using ProjectStructureTask.Services.Repositories;
using System;

namespace ProjectStructureTask.Tests.Fake
{
    public class FakeUnitOfWork : DbContext, IUnitOfWork
    {
        public FakeRepository<Project> Projects { get; } = new FakeRepository<Project>();
        public FakeRepository<Task> Tasks { get; } = new FakeRepository<Task>();
        public FakeRepository<User> Users { get; } = new FakeRepository<User>();
        public FakeRepository<Team> Teams { get; } = new FakeRepository<Team>();

        public FakeUnitOfWork()
        { }
    }
}
